# README #

# CS595 S19 Assignment 1 #

Neural networks implemented in python using Keras for classification and regression problems.

We have the following files in the src directory

##-1. AS1.ini

   This is a configuration file which contains settings for the 
   various tasks in this assignment.

   This file contains the following sections.

   [Task 1]
   modelFile = dnn_images_nn.json
   modelWeightsFile = dnn_model.h5
   retrain_model = False

   [Task 2]
   modelFile = spam_nn.json
   modelWeightsFile = spam_model.h5
   retrain_model = False

   [Task 3]
   modelFile = crime_nn.json
   modelWeightsFile = crime_model.h5
   retrain_model = False

   Each section corresponds to one task and contains the 
   following keys.
   1. modelFile : The name of the model file. 
      If this file does not exist when the python program
      starts up, it creates and trains the model. 
      The model is saved to disk as a json file with this name.

   2. modelWeightsFile: The name of the file containing model 
      weights for each model. 

   If these files exist when the corresponding python notebook 
   executes, then the notebook by default loads and configures 
   the model using the information contained in these files. 
   The model is then executed on the test data in the 
   assignment.

   To retrain the model please set the retrain_model flag to 
   True.

   Please note that the model file and the weights file should 
   be in the src directory.

##-2. dnn_images.ipynb.
   This file contains the python sources for Task 1 which is to 
   classify a subset of three images from the CIFAR-10 dataset. 
   The dataset for this assignment is provided by the Keras 
   library.
   
##-3. spam_nn.ipynb
   This file contains the python sources for Task 2 which is to 
   classify emails from the UCI dataset as spam or not spam. The 
   dataset for this assignment is available at the following 
   URL.
   url = "https://archive.ics.uci.edu/ml/machine-learning-databases/spambase/spambase.data"
   
   The program downloads the CSV file at this URL on startup. 
   Please ensure that this program is executed on a machine 
   which has internet connectivity.

##-4. crime_nn.ipynb
   This file contains the python sources for Task 3 which is 
   prediction of crime given the UCI crime dataset. This is a 
   regression problem. The dataset for this assignment is 
   available at the following URL. 
   url = "https://archive.ics.uci.edu/ml/machine-learning-databases/communities/communities.data"

   The program downloads the CSV file at this URL on startup. 
   Please ensure that this program is executed on a machine 
   which has internet connectivity.
