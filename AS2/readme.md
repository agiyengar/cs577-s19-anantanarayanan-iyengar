# README #

# CS595 S19 Assignment 2 #

Three layer neural network implementation in python for three class classification.

We have the following files in the src directory

##-1. network.py
   This file contains the NeuralNetwork class and other supporting classes like
   LayerBase, SigmoidLayer SoftmaxLayer etc. The NeuralNetwork class provides functionality
   for creating a feed forward neural network.

##-2. neural_network_tests.ipynb
    This is a python notebook file which, contains the classification test cases used for
    this assignment. This program also uses the NeuralNetwork class to perform 2 class
    classification for the UCI spam dataset. Please ensure that this program is executed on a
    machine which has network connectivity.

### Environment
Operating system : Windows 10. Version 1803.
Python Version : 3.6.0 (Anaconda 4.3.1 64 bit), Jupyter notebook.

### Program execution
The classification tests using the neural network class defined above are defined in an ipython
notebook named neural_network_tests.ipynb. To open this file, please clone the associated git folder
AS2 into a local repository and launch the jupyter notebook in an Anaconda command prompt. Please
make sure that the working directory of the jupyter  environment is set correctly via the
--notebook-dir command line parameter.

For e.g. jupyter notebook --notebook-dir="/Users/foo/temp/cs577/AS2"
The above command should launch a browser window. Please open the neural_network_tests.ipynb file
and execute it in the jupyter environment.
