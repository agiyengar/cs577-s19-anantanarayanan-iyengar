import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from enum import Enum
from keras.utils import np_utils
import matplotlib.pyplot as plt
import math
from numpy.random import RandomState

# This class defines an enum which identifies the layer type being created for the network
class LayerType(Enum):
    SIGMOID = 1
    SOFTMAX = 2

# This is the base class for the layers supported by our neural network. It defines an interface which
# is implemented by its subclasses.
class LayerBase:
    def __init__(self, input_shape, output_shape):
        self.input_shape = input_shape
        self.output_shape = output_shape
        self.output = []
        self.X = []

    # Functionality for pushing the values forward in the network goes here.        
    def feed_forward(self):
        self.not_implemented("feedforward()")

    # Functionality for computing the effective gradient using the local and incoming
    # gradients goes here.
    def back_prop(self, Y, prev_delta, learning_rate):
        self.not_implemented("backprop()")
        
    # The activation values are computed  here.
    def activate(self, val):
        self.not_implemented("activate()")
        
    # Derivative of the layer is computed here.
    def derivative(self):
        self.not_implemented("derivative()")

    # Returns the layer type.
    def layer_type(self):
        self.not_implemented("layer_type()")
        
    # Returns the layer dimensions
    def dimensions(self):
        return self.input_shape, self.output_shape
    
    # Returns the layer weights.
    def get_weights(self):
        not_implemented("get_weights()")
        
    def not_implemented(self, method):
        print(method, "not implemented")

    # https://timvieira.github.io/blog/post/2014/02/11/exp-normalize-trick/
    def exp_normalize(self, x):
        b = x.max()
        y = np.exp(x - b)
        return y / y.sum()

# Sigmoid layer subclass for the neural network. Provides functionality for managing the weights for
# a sigmoid layer instance and for computing the values pushed forward in the forward pass with the weights
# being updated in the backward pass.
class SigmoidLayer(LayerBase):
    def __init__(self, input_shape, output_shape):
        print("In SigmoidLayer constructor\n")
        self.weights = np.random.randn(input_shape, output_shape)
        self.bias = np.random.randn(output_shape)
        print("Weights shape is ", self.weights.shape)
        super().__init__(input_shape, output_shape)

    # Sigmoid activation function.
    # https://timvieira.github.io/blog/post/2014/02/11/exp-normalize-trick/
    def activate(self, val):
        return 1 / (1 + np.exp(-val))
    
    # Sigmoid derivative.
    def derivative(self):
        return self.output * (1 - self.output)
        
    def feed_forward(self, X):
        #print("In SigmoidLayer::feedforward()")
        val = np.dot(X, self.weights) + self.bias
        self.output = self.activate(val)
        #print("Sigmoid output shape is")
        #print(self.output.shape)
        self.X = X
        return self.output

    def back_prop(self, Y, prev_delta, learning_rate):
        delta = prev_delta * self.output * (1 - self.output)
        self.weights = self.weights - learning_rate * self.X.T.dot(delta)
        self.bias = self.bias - learning_rate * (delta).sum(axis=0)
        return delta
    
    def layer_type(self):
        return LayerType.SIGMOID
    
    def get_weights(self):
        return self.weights
    
# Softmax layer subclass for the neural network. Provides functionality for managing the weights for
# a sigmoid layer instance and for computing the values pushed forward in the forward pass with the weights
# being updated in the backward pass.
class SoftmaxLayer(LayerBase):
    def __init__(self, input_shape, output_shape):
        print("In SoftmaxLayer constructor\n")
        self.weights = np.random.randn(input_shape, output_shape)
        self.bias = np.random.randn(output_shape)
        print("Weights shape is", self.weights.shape)
        super().__init__(input_shape, output_shape)
        
    def set_desired_outputs(self, desired_outputs):
        self.desired_outputs = desired_outputs
        
    def activate(self, x):
        # Compute softmax values for each sets of scores in x.
        #exps = np.exp(x - np.max(x))
        #return exps / np.sum(exps)        
        exps = np.exp(x)
        return exps / exps.sum(axis=1, keepdims=True)
        
    # https://math.stackexchange.com/questions/945871/derivative-of-softmax-loss-function
    def derivative(self):
        return self.output - self.desired_outputs

    def feed_forward(self, X):
        #print("In SoftmaxLayer::feedforward()")
        self.X = X
        self.output = self.activate(X.dot(self.weights) + self.bias)
        #print("Softmax output shape is")
        #print(self.output.shape)
        return self.output
    
    def back_prop(self, Y, prev_delta, learning_rate):
        self.set_desired_outputs(Y)
        delta = self.derivative()
        self.weights = self.weights - learning_rate * self.X.T.dot(delta)
        self.bias = self.bias - learning_rate * (delta).sum(axis=0)
        return delta
    
    def layer_type(self):
        return LayerType.SOFTMAX

    def get_weights(self):
        return self.weights

def categorical_cross_entropy(Y, Y_pred):
  loss = np.sum(-Y * np.log(Y_pred))
  return loss

# This class provides functionaity for creating a neural network. This includes instantiating the network
# adding layers to it, operations like feed forward and feed backward, etc. We only support SIGMOID and SOFTMAX as the layer types.
class NeuralNetwork:
    def __init__(self, loss_function = categorical_cross_entropy):
        self.input = []
        self.outputs = []
        self.layers = []
        self.loss_function = loss_function

    # Provides functionality to add a layer identified by its type |layer_type| and dimensions given by
    # |input_shape| and |output_shape|.
    def addLayer(self, layer_type, input_shape, output_shape):
        if (layer_type == LayerType.SIGMOID):
            print("Adding SIGMOID layer with input and output shapes as ", input_shape, output_shape)
            self.layers.append(SigmoidLayer(input_shape, output_shape))
        elif (layer_type == LayerType.SOFTMAX):
            print("Adding SOFTMAX layer with input and output shapes as ", input_shape, output_shape)
            self.layers.append(SoftmaxLayer(input_shape, output_shape))

    # Provides functionality to dump information abot the network. This includes layer types, their dimensions, etc.            
    def dumpLayerInfo(self):
        print("\n*** Dumping layer details")
        for layer in self.layers:
            print("Layer type: ", layer.layer_type())
            print("Layer dimensions: ", layer.dimensions())

    # Provides functionality to push values through the network. We loop over all layers from first to last and call each
    # layers feed_forward() function which pushes its value through.
    def feed_forward(self, X):
        #print("\n*** Feedforward pass")
        current_input = X
        output = []
        for layer in self.layers:
            output = layer.feed_forward(current_input)
            #print("Layer ", layer.layer_type(), "returned")
            #print(output.shape)
            current_input = output
        return output
            
    # Provides functionality to propagate gradients through the network in reverse order, i.e from last layer to first.
    def back_prop(self, Y, learning_rate):
        total_layers = len(self.layers)
        #print("Total layers are ", total_layers)
        prev_delta = 1
        for i in range(total_layers - 1, 0, -1):
            delta = self.layers[i].back_prop(Y, prev_delta, learning_rate)
            prev_delta = (delta).dot(self.layers[i].get_weights().T)

    # Provides functionality to compute the loss for the network. We support plugging in various loss functions in
    # the network. Default is categorical cross entropy.
    def loss(self, Y, Y_pred):
        return self.loss_function(Y, Y_pred)
    
    # Provides functionality to run predictions for the records passed in.
    def predict(self, X, Y):
        Y_pred = self.feed_forward(X)
        return Y_pred

    # https://stackoverflow.com/questions/4601373/better-way-to-shuffle-two-numpy-arrays-in-unison
    # Copied from the above link.
    def unison_shuffled_copies(self, a, b):
      assert len(a) == len(b)
      p = np.random.permutation(len(a))
      return a[p], b[p]
    
    # Provides functionality for training the network. The network is trained for }epochs},
    # with |learning_rate|. The |threshold| parameter controls when we stop.
    # This function implements stochastic gradient descent with a default batch size of 16.
    def train(self, X, Y, epochs, learning_rate, threshold, batch_size = 1):
        print("\ntrain() epochs: %d, learning_rate: %f" % (epochs, learning_rate))

        X, Y = self.unison_shuffled_copies(X, Y)

        Y = self.encode_output(Y)
        losses = [0] * epochs
        
        for epoch in range(epochs):
            prev_loss = -1
            epoch_loss = []
            for i in range(0, X.shape[0], batch_size):
                input_slice = X[i:i + batch_size]
                output_slice = Y[i:i + batch_size]
                Y_pred = self.feed_forward(input_slice)
                self.back_prop(output_slice, learning_rate)
                loss = self.loss(output_slice, Y_pred)
                epoch_loss.append(loss)
            losses[epoch] = np.average(epoch_loss)
            if (prev_loss != -1):
                diff = abs(prev_loss - losses[epoch])
                if (diff <= threshold):
                    print("Difference %f between current loss %f and previous_loss %f is less than threshold %f" %(diff, loss, prev_loss, threshold))
                    break
            prev_loss = losses[epoch]
        fig = plt.figure()
        plt.plot(np.arange(0, epochs), losses)
        fig.suptitle("Training Loss")
        plt.xlabel("Epoch #")
        plt.ylabel("Loss")            
        #plt.plot(costs)
        plt.show()

    # Converts Y to one hot encoding.
    def encode_output(self, Y):
        label_encoder = LabelEncoder()
        integer_encoded = label_encoder.fit_transform(Y)
        # binary encode
        onehot_encoder = OneHotEncoder(sparse=False)
        integer_encoded = integer_encoded.reshape(len(integer_encoded), 1)
        onehot_encoded = onehot_encoder.fit_transform(integer_encoded)
        Y = onehot_encoded
        return Y
