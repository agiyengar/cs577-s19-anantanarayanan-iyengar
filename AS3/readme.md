# README #

# CS595 S19 Assignment 3 #

Neural networks implemented in python using Keras for classification and regression problems to explore
the topics of loss, optimization and regularization. 

We have the following files in the src directory

##-1. Abalone_Age_Classification.ipynb
   This python notebook contains the sources for the multiclass classification portion of 
   this assignment. We use the UCI Abalone dataset available here http://archive.ics.uci.edu/ml/datasets/abalone 
   for this purpose 

##-2. Airfoil_Noise_Prediction.ipynb
   This python notebook contains the sources for the regression portion of this assignment. We use
   the UCI Airfoil noise dataset available here https://archive.ics.uci.edu/ml/datasets/airfoil+self-noise
   for this purpose

Please ensure that the above notebooks are executed on a machine which has internet connectivity.

### Environment
Operating system : Windows 10. Version 1803.
Python Version : 3.6.0 (Anaconda 4.3.1 64 bit), Jupyter notebook.

### Program execution
Please clone the AS3 git folder into a local repository and launch the jupyter notebook from an Anaconda
command prompt. Please make sure that the working directory of the jupyter  environment is set correctly via the
--notebook-dir command line parameter.

For e.g. jupyter notebook --notebook-dir="/Users/foo/temp/cs577/AS3"

The above command should launch a browser window. Please open the above python notebooks and execute them

