# README #

# CS595 S19 Assignment 4 #

Convolutional Neural networks implemented in python using Keras for classification problems.

We have the following files in the src directory

##-1. kaggle_dogs_and_cats_classification.ipynb
   This python notebook contains the sources for the programming question 1 which is a binary classification
   problem using the Kaggle Cats and dogs dataset. Please refer to the data.txt file in the data folder
   for instructions on how to download this dataset.

##-2. cifar10_multiclass_classification.ipynb
   This python notebook contains the sources for the programming question 2 which is the multi class classification
   problem using the cifar10 dataset. Please refer to the data.txt file in the data folder
   for instructions on how to download this dataset.

Please ensure that the above notebooks are executed on a machine which has internet connectivity.

### Environment
Operating system : Windows 10. Version 1803, AWS Ubuntu 4 GPU 64 CPU instance.
Python Version : 3.6.0 (Anaconda 4.3.1 64 bit), Jupyter notebook.

### Program execution
Please clone the AS5 git folder into a local repository and launch the jupyter notebook from an Anaconda
command prompt. Please make sure that the working directory of the jupyter  environment is set correctly via the
--notebook-dir command line parameter.

For e.g. jupyter notebook --notebook-dir="/Users/foo/temp/cs577/AS4"

The above command should launch a browser window. Please open the above python notebooks and execute them

