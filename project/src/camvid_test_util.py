import cv2
from scipy import misc
import utils
import matplotlib.pyplot as plt
import shutil
import random
import os
import numpy as np
import os
import camvid_data_manager as cdm
import segnet_model
import kaggle_data_generator as kdg
import utils
from keras.callbacks import TensorBoard
from sklearn.metrics import classification_report
from utils import init_logging, log_debug, log_info, log_warning, log_critical, log_exception

ini_path = "../image_segmentation.ini"

# https://github.com/0bserver07/Keras-SegNet-Basic/blob/master/Segnet-Evaluation-Visualization.ipynb
Sky = [128, 128, 128]
Building = [128, 0, 0]
Pole = [192, 192, 128]
Road_marking = [255, 69, 0]
Road = [128, 64, 128]
Pavement = [60, 40, 222]
Tree = [128, 128, 0]
SignSymbol = [192, 128, 128]
Fence = [64, 64, 128]
Car = [64, 0, 128]
Pedestrian = [64, 64, 0]
Bicyclist = [0, 128, 192]
Unlabelled = [0, 0, 0]

label_colours = np.array(
    [Sky, Building, Pole, Road, Pavement, Tree, SignSymbol, Fence, Car,
     Pedestrian, Bicyclist, Unlabelled])

# https://github.com/0bserver07/Keras-SegNet-Basic/blob/master/Segnet-Evaluation-Visualization.ipynb


def convert_mask_to_rgb(mask):
    r = mask.copy()
    g = mask.copy()
    b = mask.copy()

    for l in range(0, 11):
        r[mask == l] = label_colours[l, 0]
        g[mask == l] = label_colours[l, 1]
        b[mask == l] = label_colours[l, 2]

    rgb = np.zeros((mask.shape[0], mask.shape[1], 3))
    rgb[:, :, 0] = (r/255.0)  # [:,:,0]
    rgb[:, :, 1] = (g/255.0)  # [:,:,1]
    rgb[:, :, 2] = (b/255.0)  # [:,:,2]
    return rgb


def load_model(optimizer="optimizers.Adam(lr=0.0001)", weights=None):
    segnet_instance = segnet_model.SegnetModel(
        ini_path, "Camvid")
    model = segnet_instance.load_model(
        optimizer, loss_type=segnet_model.LossType.CROSS_ENTROPY_LOSS,
        num_classes=12, weights=weights)
    if (model == None):
        log_warning(
            "Failed to load model. Please train the model first before continuing further tests.")

    log_info("Exiting Loaded model")
    return model

# Returns a random collection of image and mask paths of size |count|


def generate_image_and_mask_paths(folder, count):
    data_path = utils.get_config_value(
        ini_path, "Camvid", "dataPath")
    seed = int(
        utils.get_config_value(
            ini_path, "Camvid", "imageAugmentationSeed"))
    image_file_names = random.sample(
        os.listdir(data_path + "/" + folder), count)

    image_paths = []
    mask_paths = []
    for image_file_name in image_file_names:
        image_paths.append(data_path + "/" + folder + "/" + image_file_name)
        mask_paths.append(data_path + "/" + folder + "annot/" + image_file_name)

    return image_paths, mask_paths

# Helper function to test #count images in the |folder|. The function loads the images and
# calls the predict() function on the model loaded above. The returned mask from predict is plotted
# along with the image, actual mask and the predicted mask.


def test_images(model, folder, count):
    data_path = utils.get_config_value(
        ini_path, "Camvid", "dataPath")
    image_width = int(utils.get_config_value(
        ini_path, "Camvid", "imageWidth"))
    image_height = int(utils.get_config_value(
        ini_path, "Camvid", "imageHeight"))
    seed = int(
        utils.get_config_value(
            ini_path, "Camvid", "imageAugmentationSeed"))
    log_info("Image width %d, Image height %d" % (image_width, image_height))
    log_info("datapath %s" % (data_path))

    image_paths, mask_paths = generate_image_and_mask_paths(folder, count)

    for i in range(0, len(image_paths)):
        image_path = image_paths[i]
        log_info("Image path is %s" % image_path)
        image = misc.imread(image_path)
        image = cv2.resize(image, (image_width, image_height))
        image_normalized = np.array(image, np.float32) / 255

        predicted = model.predict(np.expand_dims(image_normalized, axis=0))
        mask = np.argmax(predicted, axis=-1)
        mask = np.asarray(convert_mask_to_rgb(np.squeeze(mask)))

        original_mask = misc.imread(mask_paths[i])
        original_mask = cv2.resize(original_mask, (image_width, image_height))
        original_mask = np.asarray(
            convert_mask_to_rgb(np.squeeze(original_mask)))

        images = []
        cmaps = []

        images.append(image)
        images.append(original_mask)
        images.append(mask)

        cmaps.append(None)
        cmaps.append(None)
        cmaps.append(None)

        utils.plot_images(images, cmaps)
        

def populate_original_and_predicted_masks(model,folder = "test"):
    data_path = utils.get_config_value(ini_path, "Camvid", "dataPath")
    image_width = int(utils.get_config_value(ini_path, "Camvid", "imageWidth"))
    image_height = int(utils.get_config_value(ini_path, "Camvid", "imageHeight"))
    seed = int(utils.get_config_value(ini_path, "Camvid", "imageAugmentationSeed"))
    log_info("Image width %d, Image height %d" %(image_width, image_height))
    log_info("datapath %s" %(data_path))
    
    original_mask = []
    predicted_masks = []
    for image_path in os.listdir(data_path + "/" + folder):
        
        image = misc.imread(data_path + "/" + folder + "/" + image_path)
        image = cv2.resize(image, (image_width, image_height))
        image_normalized = np.array(image, np.float32) / 255
        predicted = model.predict(np.expand_dims(image_normalized, axis = 0))
        predicted_mask = np.argmax(predicted, axis = -1)
        predicted_masks.append(predicted_mask)
        
        original_mask_folder = folder + "annot"
        image_original_mask = misc.imread(data_path + "/" + original_mask_folder + "/" + image_path)
        image_original_mask = cv2.resize(image_original_mask, (image_width, image_height))
        
        original_mask.append(image_original_mask)


    return  original_mask, predicted_masks

def camvid_classification_report(original_mask,  predicted_masks):
    predicted = []
    actual = []
    for i in range(len(predicted_masks)):
        p = np.array(predicted_masks[i])
        a = np.array(original_mask[i]) 
        predicted.extend( np.reshape(p, (np.product(p.shape),)))
        actual.extend( np.reshape(a, (np.product(a.shape),)))
        target_names = ["Sky", "Building", "Pole", "Road", "Pavement", "Tree", 
                            "SignSymbol", "Fence", "Car", "Pedestrian", "Bicyclist", "Unlabelled"]
    log_info(classification_report(actual,predicted, target_names=target_names))
