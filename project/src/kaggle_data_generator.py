from keras.preprocessing.image import ImageDataGenerator
from enum import Enum

class BatchType(Enum):
    TRAIN_DATA = 1
    VALIDATION_DATA = 3

class KaggleDataGenerator:
    def __init__(self, batch_type,  fill_mode = 'nearest', cval = 0):
        self.data_gen = ImageDataGenerator(rotation_range=40, 
                                           width_shift_range=0.2,
                                           height_shift_range=0.2,
                                           shear_range=0.2,
                                           zoom_range=0.2,
                                           horizontal_flip=True,
                                           fill_mode=fill_mode,
					   cval = cval)
    
    def getDatagenerator(self):
        return self.data_gen
