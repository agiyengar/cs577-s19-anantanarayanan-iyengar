import numpy as np
import os
from enum import Enum
from keras.layers import *
from keras.models import Model, load_model, model_from_json
from keras.optimizers import *
from keras import backend
from keras import optimizers
import keras.losses
import utils
import sklearn
import tensorflow as tf
from utils import init_logging, log_debug, log_info, log_warning, log_critical, log_exception
from keras.utils.training_utils import multi_gpu_model

# https://github.com/PavlosMelissinos/enet-keras/blob/master/src/models/layers/pooling.py
# Segnet model uses max pooling indices computed in the encoder network to perform upsampling
# in the decoder layer. This class is based on the implementation in the above link. It returns
# the max pool kernel along with the corresponding indices
class EncoderMaxPool2D(Layer):
    def __init__(self, pool_size=(2, 2), strides=(2, 2), padding='valid', **kwargs):
        super(EncoderMaxPool2D, self).__init__(**kwargs)
        self.padding = padding
        self.pool_size = pool_size
        self.strides = strides

    def call(self, inputs, **kwargs):
        log_info("In EncoderMaxPool2D._pooling_function()")
        padding = self. padding.upper()
        ksize = [1, self.pool_size[0], self.pool_size[1], 1]
        strides = [1, self.strides[0], self.strides[1], 1]

        output, max_index = tf.nn.max_pool_with_argmax(inputs, ksize = ksize, strides = strides, padding = padding)
        max_index = K.cast(max_index, K.floatx())
        return [output, max_index]

    def compute_output_shape(self, input_shape):
        ratio = (1, 2, 2, 1)
        output_shape = [
                dim//ratio[idx]
                if dim is not None else None
                for idx, dim in enumerate(input_shape)]
        output_shape = tuple(output_shape)
        return [output_shape, output_shape]

    def compute_mask(self, inputs, mask=None):
        return 2 * [None]

# https://github.com/PavlosMelissinos/enet-keras/blob/master/src/models/layers/pooling.py
# This class is baed on the corresponding implementation in the above link. It uses the max pooling
# indices computed in the encoder for upsampling.
class UpsampleUsingMaxPoolingIndices(UpSampling2D):
    def __init__(self, size=(2, 2), data_format=None, interpolation='nearest',
                 **kwargs):
        super(UpsampleUsingMaxPoolingIndices, self).__init__(size, **kwargs)
        self.interpolation = interpolation

    def call(self, inputs, output_shape = None):
        log_info("In UpsampleUsingMaxPoolingIndices.call()")
        updates, mask = inputs[0], inputs[1]
        with K.tf.variable_scope(self.name):
            mask = K.cast(mask, 'int32')
            input_shape = K.tf.shape(updates, out_type='int32')
            #  calculation new shape
            if output_shape is None:
                output_shape = (input_shape[0], input_shape[1] * self.size[0], input_shape[2] * self.size[1], input_shape[3])
            self.output_shape1 = output_shape

            # calculation indices for batch, height, width and feature maps
            one_like_mask = K.ones_like(mask, dtype='int32')
            batch_shape = K.concatenate([[input_shape[0]], [1], [1], [1]], axis=0)
            batch_range = K.reshape(K.tf.range(output_shape[0], dtype='int32'), shape=batch_shape)
            b = one_like_mask * batch_range
            y = mask // (output_shape[2] * output_shape[3])
            x = (mask // output_shape[3]) % output_shape[2]
            feature_range = K.tf.range(output_shape[3], dtype='int32')
            f = one_like_mask * feature_range

            # transpose indices & reshape update values to one dimension
            updates_size = K.tf.size(updates)
            indices = K.transpose(K.reshape(K.stack([b, y, x, f]), [4, updates_size]))
            values = K.reshape(updates, [updates_size])
            ret = K.tf.scatter_nd(indices, values, output_shape)
            return ret

    def compute_output_shape(self, input_shape):
        mask_shape = input_shape[1]
        return (
                mask_shape[0],
                mask_shape[1]*self.size[0],
                mask_shape[2]*self.size[1],
                mask_shape[3]
                )

# This class defines an enum which identifies the loss function used by the Segnet model
class LossType(Enum):
    DICE_LOSS = 1
    CROSS_ENTROPY_DICE_LOSS = 2
    CROSS_ENTROPY_LOSS = 3

# Provides functionality for creating a Segnet model.
# Incomplete.
class SegnetModel:
    def __init__(self, config_file_name, section_name = "General"):
        init_logging(config_file_name)
        self.config_file_name = config_file_name
        self.model = []
        self.loss_function = self.dice_loss
        self.loss_type = LossType.DICE_LOSS
        self.image_width = int(utils.get_config_value(config_file_name, section_name, "imageWidth"))
        self.image_height = int(utils.get_config_value(config_file_name, section_name, "imageHeight"))
        self.image_channels = int(utils.get_config_value(config_file_name, section_name, "imageChannels"))
        self.model_file_name = utils.get_config_value(config_file_name, section_name, "modelFile")
        self.model_weights_file_name = utils.get_config_value(config_file_name, section_name, "modelWeightsFile")
        self.retrain_model = int(utils.get_config_value(config_file_name, section_name, "retrainModel"))
        self.number_gpus = 0
        self.concatenate_during_decoding = int(utils.get_config_value(config_file_name, section_name, "concatenateDuringDecoding"))
        gpus = utils.get_config_value("image_segmentation.ini", section_name, "gpus")
        if (gpus != None and gpus != "-1"):
            self.number_gpus = int(gpus)

        log_info("Total number of gpus %d" %self.number_gpus)
        self.use_max_pooling = int(utils.get_config_value(config_file_name, section_name, "UseMaxPoolingIndices"))
        log_info("Use MaxPoolingIndices %d" %self.use_max_pooling)
    
    # Provides functionality to build the Segnet like model.
    # Please refer here for details 
    # https://arxiv.org/pdf/1511.00561.pdf
    # https://www.pyimagesearch.com/2017/03/20/imagenet-vggnet-resnet-inception-xception-keras/
    def build(self, optimizer = "optimizers.Adam()", loss_type = LossType.DICE_LOSS, num_classes = 1, weights = None):
        inputs = Input(shape=(self.image_width, self.image_height, self.image_channels))
        # Build the encoder network
        encoder1_pool, encoder1_pool_index, encoder1_result = self.add_encoder_layer(64, (3, 3), inputs, 2)
        encoder2_pool, encoder2_pool_index, encoder2_result = self.add_encoder_layer(128, (3, 3), encoder1_pool, 2)
        encoder3_pool, encoder3_pool_index, encoder3_result = self.add_encoder_layer(256, (3, 3), encoder2_pool, 3)
        encoder4_pool, encoder4_pool_index, encoder4_result = self.add_encoder_layer(512, (3, 3), encoder3_pool, 3)

        # Center layer 1. (End of encoder)
        encoder5_pool, encoder5_pool_index, encoder5_result = self.add_encoder_layer(512, (3, 3), encoder4_pool, 3, (0, 0))

        # Center layer 2. (Start of decoder)
        decoder1_result = None
        if (self.use_max_pooling != 1):
            ignored1, ignored2, decoder1_result = self.add_encoder_layer(512, (3, 3), encoder5_result, 3, (0, 0))
        else:
            decoder1_result = self.add_decoder_layer(512, (3, 3), encoder5_result, 3, encoder5_result, encoder5_pool, encoder5_pool_index)

        decoder2_result = self.add_decoder_layer(512, (3, 3), decoder1_result, 3, encoder4_result, encoder4_pool, encoder4_pool_index)
        decoder3_result = self.add_decoder_layer(256, (3, 3), decoder2_result, 2, encoder3_result, encoder3_pool, encoder3_pool_index)
        decoder4_result = self.add_decoder_layer(128, (3, 3), decoder3_result, 2, encoder2_result, encoder2_pool, encoder2_pool_index)
        decoder5_result = self.add_decoder_layer(64, (3, 3), decoder4_result, 2, encoder1_result, encoder1_pool, encoder1_pool_index)

        self.num_classes = num_classes
        classification = None
        if (num_classes == 1):
            log_info("Adding sigmoid classification layer")
            classification = Conv2D(num_classes, (1, 1), padding = 'valid', name = "output_layer")(decoder5_result)
            classification = BatchNormalization()(classification)
            classification = keras.layers.Activation("sigmoid")(classification)
        else:
            log_info("Adding softmax classification layer")
            classification = Conv2D(num_classes, (1, 1), padding = 'valid', name = "output_layer")(decoder5_result)
            classification = BatchNormalization()(classification)
            classification = keras.layers.Activation("softmax")(classification)

        self.model = Model(inputs = inputs, outputs = classification)
        metrics = self.dice_coef
        self.weights = weights
        loss_function = None
        if (loss_type == LossType.DICE_LOSS):
            log_info("Building model with DICE loss function")
            loss_function = self.dice_loss
        elif (loss_type == LossType.CROSS_ENTROPY_LOSS):
            log_info("Building model with Cross entropy loss function")
            if (num_classes == 1):
                log_info("Using binary_crossentropy as the loss function")
                loss_function = keras.losses.binary_crossentropy
            else:
                metrics = SegnetModel.categorical_accuracy
                if (self.weights is not None):
                    log_info("Using weighted sparse_categorical_crossentropy as the loss function")
                    loss_function = self.weighted_sparse_categorical_crossentropy
                else:
                    log_info("Using sparse_categorical_crossentropy as the loss function")
                    loss_function = keras.losses.sparse_categorical_crossentropy
        else:
            log_info("Building model with cross entropy DICE loss function")
            loss_function = self.cross_entropy_dice_loss
        if (self.number_gpus != 0):
            self.multi_gpu_model = multi_gpu_model(self.model, gpus = self.number_gpus)
            self.multi_gpu_model.compile(loss = loss_function, optimizer = eval(optimizer), 
                                         metrics = [metrics])
        self.model.compile(loss = loss_function, optimizer = eval(optimizer), metrics=[metrics])
        if (self.number_gpus != 0):
            return self.multi_gpu_model
        return self.model

    # https://machinelearningmastery.com/save-load-keras-deep-learning-models/
    def save(self):
        model_json = self.model.to_json()

        model_path = os.path.dirname(self.model_file_name)
        utils.mkdir_p(model_path)
   
        with open(self.model_file_name, "w") as json_file:
            json_file.write(model_json)
        
        self.model.save_weights(self.model_weights_file_name)

        log_info("\n Saved model structure to model file %s and weights to %s" %(self.model_file_name, self.model_weights_file_name))

    # https://machinelearningmastery.com/save-load-keras-deep-learning-models/    
    def load_model(self, optimizer = "optimizers.Adam()", loss_type = LossType.DICE_LOSS, num_classes = 1, weights = None):
        try:
            if (self.retrain_model == 1):
                log_info("Not loading existing weights as model is being retrained.")
                return None

            json_file = open(self.model_file_name, 'r')
            log_info("\nOpened model file %s" %self.model_file_name)
            loaded_model_json = json_file.read()
            json_file.close()

            loaded_model = model_from_json(loaded_model_json, 
                                           custom_objects = {'EncoderMaxPool2D': EncoderMaxPool2D, 'UpsampleUsingMaxPoolingIndices': UpsampleUsingMaxPoolingIndices})
            log_info("\nLoading weights from file %s" %self.model_weights_file_name)
            # load weights into new model
            loaded_model.load_weights(self.model_weights_file_name)
            self.model = loaded_model
            self.num_classes = num_classes
            self.weights = weights
            metrics = self.dice_coef
            loss_function = None
            if (loss_type == LossType.DICE_LOSS):
                loss_function = self.dice_loss
            elif (loss_type == LossType.CROSS_ENTROPY_LOSS):
                if (num_classes == 1):
                    loss_function = keras.losses.binary_crossentropy
                else:
                    metrics = SegnetModel.categorical_accuracy
                    if (self.weights is not None):
                        log_info("Using weighted sparse_categorical_crossentropy as the loss function")
                        loss_function = self.weighted_sparse_categorical_crossentropy
                    else:
                        log_info("Using sparse_categorical_crossentropy as the loss function")
                        loss_function = keras.losses.sparse_categorical_crossentropy
            else:
                loss_function = self.cross_entropy_dice_loss
            self.model.compile(loss = loss_function, optimizer = eval(optimizer), metrics=[metrics])
            return self.model

        except IOError:
            log_exception("\nError occurred while reading model file %s" %self.model_file_name)
            return None

    def add_encoder_layer(self, channels, filter_size, layer_input, total_layers, max_pooling_size = (2, 2)):
        input = layer_input
        encoder_result = None
        batch_norm = None

        for i in range(0, total_layers):
          encoder = Conv2D(channels, filter_size, padding = "same")(input)
          batch_norm = BatchNormalization()(encoder)
          encoder_result = Activation('relu')(batch_norm)
          input = encoder_result

        if (self.use_max_pooling != 1 and max_pooling_size == (0, 0)):
            return None, None, encoder_result

        encoder_pool, max_pool_index = EncoderMaxPool2D(max_pooling_size, strides = (2,2))(batch_norm)
        return encoder_pool, max_pool_index, encoder_result

    def add_decoder_layer(self, channels, filter_size, layer_input, total_layers, encoder_result, encoder_pool = None,
                          encoder_pool_index = None):
        input = layer_input
        decoder_result = None

        if (self.use_max_pooling != 1 or encoder_pool is None):
            decoder_upsampled = UpSampling2D((2, 2))(input)
            if (self.concatenate_during_decoding is None or self.concatenate_during_decoding == 1):
                decoder_upsampled = concatenate([encoder_result, decoder_upsampled], axis = 3)
            input = decoder_upsampled
        else:
            log_info("Using max pooling indices")
            input = UpsampleUsingMaxPoolingIndices((2,2))([encoder_pool, encoder_pool_index])
  
        decoder_result = None

        for i in range(0, total_layers):
          decoder = Conv2D(channels, filter_size, padding = "same")(input)
          batch_norm = BatchNormalization()(decoder)
          decoder_result = Activation('relu')(batch_norm)
          input = decoder_result

        return decoder_result

    # The dice coefficient and dice loss code were copied from the stackoverflow discussion below.
    # https://stackoverflow.com/questions/49785133/keras-dice-coefficient-loss-function-is-negative-and-increasing-with-epochs
    def dice_coef(self, y_true, y_pred):
        smooth = 1.
        y_true_f = K.flatten(y_true)
        y_pred_f = K.flatten(y_pred)
        intersection = K.sum(y_true_f * y_pred_f)
        return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)

    def dice_loss(self, y_true, y_pred):
        return 1 - self.dice_coef(y_true, y_pred)

    def cross_entropy_dice_loss(self, y_true, y_pred):
        cross_entropy_loss = 0
        if (self.num_classes == 1):
            cross_entropy_loss = keras.losses.binary_crossentropy(y_true, y_pred)
        else:
            cross_entropy_loss = keras.losses.sparse_categorical_crossentropy(y_true, y_pred)
        return cross_entropy_loss + self.dice_loss(y_true, y_pred)

    def categorical_accuracy(y_true, y_pred):
        # reshape in case it's in shape (num_samples, 1) instead of (num_samples,)
        if K.ndim(y_true) == K.ndim(y_pred):
            y_true = K.squeeze(y_true, -1)
        # convert dense predictions to labels
        y_pred_labels = K.argmax(y_pred, axis=-1)
        y_pred_labels = K.cast(y_pred_labels, K.floatx())
        return K.cast(K.equal(y_true, y_pred_labels), K.floatx())

    # This function was adapted from the tensorflow sparse categorical cross entropy implementation below.
    # https://github.com/keras-team/keras/blob/master/keras/backend/tensorflow_backend.py
    # It supports weighting the loss based on the proportion of the corresponding class weight.
    def weighted_sparse_categorical_crossentropy(self, y_true, y_pred):
        output = tf.clip_by_value(y_pred, 10e-8, 1.-10e-8)
        output = tf.log(output)

        output_shape = output.get_shape()
        targets = K.cast(K.flatten(y_true), 'int64')
        logits = K.reshape(output, [-1, int(output_shape[-1])])
        weighted_logits = tf.multiply(logits, self.weights)

        res = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=targets, logits = weighted_logits)
        if len(output_shape) >= 3:
            # if our output includes timestep dimension
            # or spatial dimensions we need to reshape
            return tf.reshape(res, tf.shape(output)[:-1])
        else:
            return res